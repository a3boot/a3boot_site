var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var nodemailer = require('nodemailer');
var randomL=require('./bin/RandomLink/index.js');
var fs = require('fs');
var sql = require('mysql');
var someq=require('./bin/conn/index.js');
var url = require('url');
var handlers = require('./bin/Form_handler/index.js');

var router = express.Router();
var routes=require('./controllers/index');
var users = require('./controllers/users');
var contacts = require('./controllers/contacts');
var about_us=require('./controllers/about_us_page');
var for_klients=require('./controllers/for_klients');
var actReg = require('./controllers/actReg');
var missPass = require('./controllers/missPassword');
var add_new_news=require('./controllers/add_new_news');
var full_news=require('./controllers/full_news');
var handler = require('./bin/addNewPageHandler');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views/pages'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'images',  'plus.png')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({secret: 'ssshhhhh1',saveUninitialized: true,resave: true}));
app.use(bodyParser.urlencoded({extended: true}));

var sess;

app.get('/ua', function (req,res,next) {
  req.session.lang = 'ua';
  res.redirect('back');
  next();
});

app.get('/en', function (req,res,next) {
  req.session.lang = 'en';
  res.redirect('back');
  next();
});


app.get('/ru', function (req,res,next) {
  req.session.lang = 'ru';
  res.redirect('back');
  next();
});

app.post('/registration', someq.checkAndInsertUsers);

app.post('/login', someq.logAjax);

app.get('/logout',function(req,res){
  delete req.session.email;
  delete req.session.user;
  delete req.session.idUser;
  delete req.session.fName;
  delete req.session.lName;
       res.redirect('/');

});

app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret: 'ssshhhhh1',saveUninitialized: true,resave: true}));
app.use(bodyParser.urlencoded({extended: true}));
app.use('/form_handler', bodyParser.urlencoded({
  extended: true
}));
app.use('/', routes);
app.use('/users', users);
app.use('/about_us_page', about_us);
app.use('/for_klients', for_klients);
app.use('/contacts', contacts);
app.use('/add_new_news', add_new_news);
app.post('/addNewPageHandler', handler.addNewPageHandler);
app.use('/full_news?:id', full_news);
app.use('/actReg?:actReg', actReg);
app.use('/missPassword?:missPass', missPass);

app.post('/form_handler', handlers.formHandler);

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}


app.post('/yourmessage', function(req, res, next) {
  //send messages to database
    someq.newMessages(req, res, next);
    res.redirect(301, '/contacts');
    next();

});

app.post('/missPassword', function(req, res, next) {

 
  //send link to database and send mail to user
  someq.newLink(req, res, next);
  someq.newLinkMissSend(req, res, next);
  res.redirect('back');
});


// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.post('/newPassword', function(req, res, next) {

  //console.log(req.body.passwordReg);
  //write new pasword
 someq.formNewPassword(req,res,next);
    
  res.redirect('/');
});



module.exports = app;

