function registration() {

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            if(xhr.responseText) {
                document.getElementById('anotherEmail').innerHTML = xhr.responseText;
            }else{
                window.location.href="/";
            }
        }else if (xhr.readyState == 4 && xhr.status != 200) {
            console.dir('error');
        }
    };
    xhr.open('POST', '/registration', true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
    var regInfo='lastName='+document.getElementById('lastName').value+'&firstName='+document.getElementById('firstName').value+
        '&phoneNumber='+document.getElementById('phoneNumber').value+'&emailReg='+document.getElementById('emailReg').value+
        '&loginReg='+document.getElementById('loginReg').value+'&passwordReg='+document.getElementById('passwordReg').value;
    xhr.send(regInfo);
}
function checkPass ()
{

    with (document) {
        var password = getElementById('passwordReg').value; // Получаем пароль из формы
        var s_letters = "qwertyuiopasdfghjklzxcvbnm"; // Буквы в нижнем регистре
        var b_letters = "QWERTYUIOPLKJHGFDSAZXCVBNM"; // Буквы в верхнем регистре
        var digits = "0123456789"; // Цифры
        var specials = "!@#$%^&*()_-+=\|/.,:;[]{}"; // Спецсимволы
        var is_s = false; // Есть ли в пароле буквы в нижнем регистре
        var is_b = false; // Есть ли в пароле буквы в верхнем регистре
        var is_d = false; // Есть ли в пароле цифры
        var is_sp = false; // Есть ли в пароле спецсимволы
        for (var i = 0; i < password.length; i++) {

            /* Проверяем каждый символ пароля на принадлежность к тому или иному типу */
            if (!is_s && s_letters.indexOf(password[i]) != -1) is_s = true;
            else if (!is_b && b_letters.indexOf(password[i]) != -1) is_b = true;
            else if (!is_d && digits.indexOf(password[i]) != -1) is_d = true;
            else if (!is_sp && specials.indexOf(password[i]) != -1) is_sp = true;
        }
        var rating = 0;
        var text = "";
        if (is_s) rating++; // Если в пароле есть символы в нижнем регистре, то увеличиваем рейтинг сложности
        if (is_b) rating++; // Если в пароле есть символы в верхнем регистре, то увеличиваем рейтинг сложности
        if (is_d) rating++; // Если в пароле есть цифры, то увеличиваем рейтинг сложности
        if (is_sp) rating++; // Если в пароле есть спецсимволы, то увеличиваем рейтинг сложности

        /* Далее идёт анализ длины пароля и полученного рейтинга, и на основании этого готовится текстовое описание сложности пароля */
        if (password.length < 6 && rating < 3) text = "Занадто простий пароль.";
        else if (password.length < 6 && rating >= 3) text = "Середний пароль";
        else if (password.length >= 8 && rating < 3) text = "Середний пароль";
        else if (password.length >= 8 && rating >= 3) text = "Хороший пароль";
        else if (password.length >= 6 && rating == 1) text = "Занадто простий пароль.";
        else if (password.length >= 6 && rating > 1 && rating < 4) text = "Середний пароль";
        else if (password.length >= 6 && rating == 4) text = "Хороший пароль";
        getElementById('bad_pass1').innerHTML = text; // Выводим итоговую сложность пароля
        //return false;

        // with (document) {
        //  getElementById('bad_pass1').innerHTML = (getElementById('pass1').value.length < 5 ) ?
        //          'Пароль закороткий!' : '';
        //  getElementById('bad_pass2').innerHTML='repeat';

    }
}
function checkPass2 ()
{
    with (document)          getElementById ('bad_pass2').innerHTML = (getElementById ('passwordReg').value != getElementById ('repeatPassword').value) ?
        'Пароль повторенй невірно!' : '';
}