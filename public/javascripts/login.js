function wrongLogin() {

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            if(xhr.responseText) {
                document.getElementById('errWrongLogin').innerHTML = xhr.responseText;
            }else{
                location.reload();
                 }
        }else if (xhr.readyState == 4 && xhr.status != 200) {
            console.dir('error');
        }
    };
    xhr.open('POST', '/login', true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
    var logInfo='email='+document.getElementById('email').value+'&password='+document.getElementById('password').value;
    xhr.send(logInfo);
}