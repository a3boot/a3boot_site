var express = require('express');
var router = express.Router();

var lang = require('../bin/langKey.js');

/* GET home page. */
router.get('/', function(req, res, next) {
    req.session.lang = req.session.lang || 'ua';
    languageSess = lang.funcLang(req.session.lang, 'contacts');

    languageSess.title = 'a3boot';
    res.render('contacts', languageSess)
});

module.exports = router;