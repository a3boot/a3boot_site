module.exports.rightSide = function (req,res,next, language) {
    sess = req.session;
    if(sess.email==undefined) {
        
        menu =
            '<div id="login_form">' +
            '<div id="login_form_title">' + '<h2>' + language.loginformhader + '</span>' + '</h2>' + '</div>' +
            '<form id="login" action="/login" method="POST">' +
            '<div id="errWrongLogin" style="color: red">'+' '+'</div>'+
            '<input type="text" name="email" id="email" placeholder="'+language.name+'" required="true">' + '<br>' + '<br>' +
            '<input type="password" name="password" id="password" placeholder="'+language.password+'">' + '<br>' + '<br>' +
            '<input type="button" name="submit" id="send" value="'+language.checkin+'" onclick="wrongLogin()">' +
            '</form>' +
            '<div id="ger_forg">' +
            '<div class="row">' +
            '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">' +
            '<div id="registration">' + '<a href="#openModal">' + language.registration + '</a>' + '</div>' +
            <!-- Registration form-->
            '<div id="openModal" class="modalDialog">' +
            '<div>' +
            '<a href="#close" title="Закрити" class="close">' + 'X' + '</a>' +
            '<h3>' + language.typeText + '</h3>' +
            '<form action="/registration" method="POST" id="regForm" autocomplete="on">' +
            '<p id="anotherEmail" style="color: red">' + ' ' + '</p>'+

            '<p>' + language.lastName +
            '<br>' + '<input type="text" id="lastName" name="lastName" required>' + '</p>' +
            '<p>' + language.name +
            '<br>' + '<input type="text" id="firstName" name="name" required>' + '</p>' +
            '<p>' + language.telephone +
            '<br>' + '<input type="tel" name="tel" id="phoneNumber" required>' + '</p>' +
            '<p>' + language.eMail +
            '<br>' + '<input type="email" name="mail" id="emailReg" required>' + '</p>' +
            '<p>' + language.login +
            '<br>' + '<input type="text" name="login" id="loginReg" required>' + '</p>' +
            '<p id="bad_pass1" style="color: red"></p>' +
            '<p>' + language.password +
            '<br>' + '<input type="password" name="password" id="passwordReg" maxlength="13" required onchange="checkPass ()">' + '</p>' +
            '<p id="bad_pass2" style="color: red">' + '</p>' +
            '<p>' + language.repeatPassword +
            '<br>' + '<input type="password" name="repeatPassword" id="repeatPassword" maxlength="13" required onchange="checkPass2 ()">' + '</p>' +
            <!-- Re-captcha -->
            '<input type="button" name="submit" id="send" value="'+language.signUp+'" onclick="registration()">' +
            '</form>' +
            '</div>' +
            '</div>' +
            <!-- End of registration form-->
            '</div>' +
            '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">' +
            '<div id="forgot_password">' + '<a href="#openModal1">' + language.forgotPassword + '</a>' + '</div>' +
            '<div id="openModal1" class="modalDialog">' +
            '<div>' +
            '<a href="#close" title="Закрити" class="close">' + 'X' + '</a>' +
            '<h3 class="lang" key="type-text">' + 'Введіть email:' + '</h3>' +
            
            '<form action="/missPassword" method="POST" id="missForm" autocomplete="on">' +
            '<p>' + '<span class="lang" key="e-mail">' + 'Email' + '</span>' +
            '<br>' + '<input type="email" name="mail" required>' + '</p>' +
            '<input type="submit" id="send" value="Надіслати" name="submit">' +
            '</form>'+'</div>'+'</div>'+
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
    }else {
        menu = '<div id="question">' +
            '<div class="row">' +
            '<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">' +
            '<div id="employee_name">' + '<h2>' + '<a href="">' + sess.fName +' '+ sess.lName + '</a>' + '</h2>' + '</div>' +
            '</div>' +
            '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">' +
            '<div id="gear_ico">' + '<img src="images/gear_ico.png" alt="">' + '</div>' +
            '</div>' +
            '</div>' +
            '<ul id="question_menu">' +
            '<li id="status_waiting">' + '<a href="">' + 'Питання з дуже-дуже довже...' + '</a>' + '</li>' +
            '<li id="status_done">' + '<a href="">' + 'Якесь виконане питання' + '</a>' + '</li>' +
            '<li id="status_denied">' + '<a href="">' + 'Питання, що відхилено' + '</a>' + '</li>' +
            '</ul>' +
            '<ul id="add_question">' +
            '<li id="plus">' + '<a href="">' +language.addQuestion + '</a>' + '</li>' +
            '</ul>' +
            '<div class="row">' +
            '<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">' +
            '<div id="all_questions">' + '<a href="">' + language.allQuestions + '</a>' + '</div>' +
            '</div>' +
            '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">' +
            '<div id="exit">' + '<a href="/logout">' + language.exit + '</a>' + '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        }
    return menu;
};

